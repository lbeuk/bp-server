CREATE TABLE device_groups
(
    gid         SERIAL PRIMARY KEY,
    parent      INTEGER REFERENCES device_groups
);

CREATE TABLE devices
(
    did         SERIAL PRIMARY KEY,
    gid         INTEGER REFERENCES device_groups NOT NULL DEFAULT 0,
    hn          VARCHAR NOT NULL,
    sn          VARCHAR,
    ip          INTEGER,
    ticket      VARCHAR NOT NULL
);

CREATE TABLE user_roles
(
    ldap_uid    INTEGER PRIMARY KEY
);

CREATE TABLE policies
(
    pid         SERIAL PRIMARY KEY,
    dev_gid     INTEGER REFERENCES device_groups NOT NULL DEFAULT 0,
    usr_gid     INTEGER,
    module      VARCHAR NOT NULL,
    key         VARCHAR NOT NULL,
    value       JSONB NOT NULL
);

INSERT INTO device_groups (gid, parent) VALUES (0, null);
