use super::schema::{device_groups, devices, user_roles, policies};
use serde_json::Value;

// admin_users table

#[derive(Queryable, Insertable)]
#[table_name = "user_roles"]
pub struct UserRole {
    pub ldap_uid: i32
}

// device_groups table

#[derive(Queryable, Insertable)]
#[table_name = "device_groups"]

pub struct DeviceGroup {
    pub gid: i32,
    pub parent: Option<i32>,
}

// devices table

#[derive(Queryable, Insertable)]
#[table_name = "devices"]
pub struct Device {
    pub did: i32,
    pub gid: Option<i32>,
    pub hn: String,
    pub sn: Option<String>,
    pub ip: i32,
    pub ticket: String
}

#[derive(Insertable)]
#[table_name = "devices"]
pub struct NewDevice {
    pub gid: Option<i32>,
    pub hn: String,
    pub sn: Option<String>,
    pub ip: i32,
    pub ticket: String
}

// policy table

#[derive(Queryable, Insertable)]
#[table_name = "policies"]
pub struct Policy {
    pub pid: i32,
    pub dev_gid: i32,
    pub usr_gid: Option<i32>,
    pub module: String,
    pub key: String,
    pub value: Value
}

#[derive(Insertable)]
#[table_name = "policies"]
pub struct NewPolicy {
    pub dev_gid: Option<i32>,
    pub usr_gid: Option<i32>,
    pub module: String,
    pub key: String,
    pub value: Value
}
