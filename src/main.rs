#![allow(clippy::type_complexity)]

use std::env;
use std::ops::Deref;

use actix_web::{App, HttpServer, middleware, web};
use diesel::PgConnection;
use diesel::r2d2::ConnectionManager;
use ldap3::LdapConn;
use r2d2::Pool;
use r2d2_ldap::LDAPConnectionManager;

use crate::services::rpc::actix_config::config;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;

mod database;
mod services {
    pub mod rpc;
}
pub mod utils {
    pub mod parser;
    pub mod ldap;
}


// Sets up shorthand types for connection pools
type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;
type LdapPool = r2d2::Pool<LDAPConnectionManager>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Define port to be used by program
    const PORT: u16 = 8080;

    // Pulls LDAP database URL from ENV
    let ldap_url= env::var("LDAP_URL").expect("LDAP_URL must be set in .env file");

    // Pulls Postgres database URL from ENV
    let postgres_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set in .env file");

    // Leaks LDAP URL for static reference
    let ldap_url= Box::leak(ldap_url.into_boxed_str()); // This is currently necessary

    // Generates LDAP connection pool via r2d2 and LDAPConnectionManager
    let ldap_pool: LdapPool = r2d2::Pool::builder()
        .build(
            LDAPConnectionManager(ldap_url))
        .expect("Could not build ldap connection pool.");

    // Generates Postgres connection pool via r2d2 and diesel
    let postgres_pool: DbPool = Pool::builder()
        .build(
            ConnectionManager::<PgConnection>::new(postgres_url))
        .expect("Could not build database connection pool.");

    // Initializes web server
    HttpServer::new(move || {
        App::new()
            // Adds the database pools for ldap and postgres
            .data(postgres_pool.clone())
            .data(ldap_pool.clone())
            // Adds the RPC service
            .service(web::scope("/rpc").configure(config))
    })
        .bind(format!("127.0.0.1:{}", PORT))
        .unwrap()
        .run()
        .await
}