use crate::utils::parser::read_conf;
use crate::LdapPool;
use ldap3::{Scope, LdapConn};
use std::env;

type LdapConnection = LdapConn;

/// Binds connection to LDAP using values set in config file
fn bind_connection(connection: &mut LdapConn) {
    let un = read_conf("ldap")["un"].as_str().expect("ldap bind username is not set in config file").to_string();
    let ps = read_conf("ldap")["ps"].as_str().expect("ldap bind password is not set in config file").to_string();

    connection.simple_bind(&un, &ps);
}

// Searches user by username (uid)
fn search_user<S>(username: S, ldap_connection: &LdapPool,) where S: Into<String> {
    let root_string = format!("ou=People,{}", get_dc().unwrap());
    let search_string = format!("(uid={})", username.into());
    let res = ldap_connection.get().unwrap().search(&root_string, Scope::Subtree, &search_string, vec!["userPassword"]).unwrap();
}

// Returns the LDAP dc sequence
fn get_dc() -> Option<String> {
    let dn1 = read_conf("ldap");
    let dn2 = dn1["ds"].as_str();
    let dn1 = dn1["dn"].as_str();

    // Verifies toml values
    let (dn1, dn2) = match (dn1, dn2) {
        (Some(dn1), Some(dn2)) => (dn1, dn2),
        (_, _) => return None
    };

    return Some(format!("dc={},dc={}", dn1, dn2));
}