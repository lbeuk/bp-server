// These functions are copied from the client crate, they should eventually be modularized into a shared crate

use toml::Value;
use std::fs;
use std::path::PathBuf;

const CONF_PATH: &str = "./bp_srv.toml";

pub fn get_abs_path(path: String) -> Option<String> {
    return Some(PathBuf::from(&path)
        .canonicalize().unwrap()
        .into_os_string().into_string().unwrap());
}

// This function is designed specifically for reading the bp_srv.toml configuration
pub fn read_conf(module: &str) -> Value {
    let conf_file = fs::read_to_string(
        get_abs_path(String::from(CONF_PATH))
            .expect("Invalid configuration path")
    )
        .expect("Something went wrong reading the file");

    let data: Value = toml::from_str(&conf_file)
        .expect("Can not parse config toml file, please check the syntax");

    return data[module].clone();
}