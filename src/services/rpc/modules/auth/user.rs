use ldap3::{LdapConnAsync, Scope, SearchEntry};
use std::{env, io};
use jsonwebtoken;
use crate::{DbPool, LdapPool};
use serde_json::Value;
use bp_conventions::conventions::rpc::ErrorData;
use crate::services::rpc::handler::RpcOut;
use crate::utils::parser::read_conf;

pub fn request_user_token(db_connection: &DbPool, ldap_connection: &LdapPool, params: &Value) -> RpcOut {
    let uid = params["un"].as_str();
    let pw = params["pw"].as_str();
    let dn1 = read_conf("domain");
    let dn2 = dn1["ds"].as_str();
    let dn1 = dn1["dn"].as_str();

    // Verifies credentials were submitted
    let (uid, pw) = match (uid, pw) {
        (Some(uid), Some(pw)) => {
            (uid, pw)
        },
        (_, _) => return Err(ErrorData::std(-32600))
    };

    println!("A");

    // Verifies toml values
    let (dn1, dn2) = match (dn1, dn2) {
        (Some(dn1), Some(dn2)) => (dn1, dn2),
        (_, _) => return Err(ErrorData::std(-32603))
    };



    // Gets ldap and db connection
    let (mut ldap_conn, mut db_conn) = match (ldap_connection.get(), db_connection.get()) {
        (Ok(ldap_conn), Ok(db_conn)) => (ldap_conn, db_conn),
        (_, _) => return Err(ErrorData::std(-32603))
    };

    // Connects to LDAP
    let x = match ldap_conn.simple_bind(
        env::var("LDAP_DN").unwrap().as_str(),
        env::var("LDAP_PASSWORD").unwrap().as_str()) {
        Ok(a) => a,
        Err(_) => return Err(ErrorData::std(-32603))
    };

    println!("{:?}", x);


    return Ok(Value::Null);
}

