use serde_json::Value;
use crate::DbPool;
use serde_json::value::Value::Null;

use crate::database::models::{Policy, NewPolicy};
use diesel::{RunQueryDsl, QueryDsl, ExpressionMethods, BoolExpressionMethods};
use crate::database::schema::policies::dsl::policies;
use crate::database::schema::policies::columns::{dev_gid, module};

pub mod dconf;



pub trait PolicyModule {
    // Searches database
    fn for_group(group_id: i32, policies: Vec<Policy>) -> Self;
    // Generates a JSON object to send over network for a particular module
    fn generate_asset(&self) -> Value;
}



#[derive(Serialize, Deserialize)]
pub struct PolicyChangeRequest {
    pub group_id: i32,
    pub module: String,
    pub key: String,
    pub value: Value
}



/// RPC function: generate_assets
///
/// # Function paramaters
/// `db_connection`: Connection pool for postgres
/// `params`: RPC parameters object
///
/// # Required RPC params
/// `policies`: List of policies to impose
///
pub fn set_policies(db_connection: &DbPool, params: &Value) -> Value {
    let policy_list = params["policies"].as_array().unwrap();
    let mut count = 0;

    for policy in policy_list {
        let parsed_policy: PolicyChangeRequest = serde_json::from_value(policy.clone()).unwrap();

        let new_policy = NewPolicy {
            dev_gid: Some(parsed_policy.group_id),
            usr_gid: None,
            module: parsed_policy.module,
            key: parsed_policy.key,
            value: parsed_policy.value
        };

        let x = diesel::insert_into(policies).values(&new_policy).execute(&db_connection.get().unwrap()).unwrap();
        count += x;
    }
    return serde_json::to_value(count).unwrap();
}



/// RPC function: generate_assets
///
/// # Arguments
///
/// * `db_connection`: Connection pool for postgres
/// * `params`: JSON value of API request paramaters
///   * `REQUIRED FIELDS`
///     * `modules`: List of policy modules to generate the assets for (ex. dconf)
///     * `group`: (Device) group ID to search policies for
pub fn generate_assets(db_connnection: &DbPool, params: &Value) {
    let modules = params["modules"].as_array().unwrap();
    let group = &params["group"].as_i64().unwrap();
    let mut json_out = serde_json::Map::new();

    for other_module in modules {
        let mod_as_str = other_module.as_str().unwrap();
        json_out.insert(mod_as_str.into(), generate_module_assets(mod_as_str, group.clone() as i32, db_connnection));
    }

}



/// RPC function: generate_module_assets
///
/// # Arguments
///
/// * `
pub fn generate_module_assets<A>(module_name: A, group_id: i32, db_connection: &DbPool) -> Value where A: Into<String> {
    let module_name = module_name.into();
    let policy_list = get_module_policies(module_name.clone(), group_id, db_connection);
    return match module_name.as_str() {
        "dconf" => dconf::DconfGroup::for_group(group_id, policy_list).generate_asset(),
        _ => Null
    }
}

pub fn get_module_policies<A>(module_name: A, group_id: i32, db_connection: &DbPool) -> Vec<Policy> where A: Into<String>{
    let module_name = module_name.into();

    return policies.filter(dev_gid.eq(group_id).and(module.eq(module_name)))
        .load::<Policy>(&db_connection.get().unwrap()).expect("Error loading policies");
}

pub fn get_assets(db_connection: &DbPool, params: &Value) -> Value {
    let modules = params["modules"].as_array().unwrap();
    let group_id: i32 = params["group_id"].as_i64().unwrap() as i32;

    let mut output = serde_json::Map::new();
    for module_val in modules {
        let module_name = String::from(module_val.as_str().unwrap());
        output.insert(module_name.clone(), generate_module_assets(module_name, group_id, db_connection));
    }

    return serde_json::Value::Object(output);
}