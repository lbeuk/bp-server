pub mod user;
pub mod device;

pub enum AuthModes {
    UnboundDevice,
    Device,
    User(u8)
}