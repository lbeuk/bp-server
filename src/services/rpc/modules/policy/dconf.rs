use serde_json::Value;
use crate::services::rpc::modules::policy::{PolicyModule, get_module_policies};
use std::collections::HashMap;
use crate::DbPool;
use crate::database::models::Policy;

pub const MODULE_NAME: &str = "dconf";

// Policy
#[derive(Clone)]
struct SplitPolicy {
    pub key: Vec<String>,
    pub value: DconfValue
}

pub struct DconfGroup {
    policies: Vec<SplitPolicy>
}

impl From<&DconfGroup> for Vec<(Vec<String>, DconfValue)> {
    fn from(o: &DconfGroup) -> Vec<(Vec<String>, DconfValue)>{
        let mut values = o.policies.iter().map(|val| {
            (val.key.clone(), val.value.clone())
        }).collect();

        return values;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum DconfValueType {
    Boolean,
    String,
    Url,
    Integer,
}

// Key-value pair for dconf settings
#[derive(Serialize, Deserialize)]
pub struct  DconfPair {
    key: String,
    value: DconfValue
}

// Abstraction for different dconf value types
// Also holds whether setting is locked
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DconfValue {
    value: String,
    value_type: DconfValueType,
    lock: bool
}

impl DconfValue {
    // Creates a new boolean dconf value
    fn boolean(v: bool) -> DconfValue {
        return DconfValue {
            value: String::from(if v {"True"} else {"False"}),
            value_type: DconfValueType::Boolean,
            lock: false
        }
    }

    // Creates a new file system url dconf value from vector of parts
    fn url(v: &Vec<String>) -> DconfValue {
        let mut output_url = String::from("file://");
        for i in v {
            output_url += i.as_str();
        }

        return DconfValue {
            value: output_url,
            value_type: DconfValueType::Url,
            lock: false
        }
    }

    // Locks the dconf value
    fn lock(mut self) -> Self{
        self.lock = true;
        return self;
    }
}

impl PolicyModule for DconfGroup {
    // Searches database
    fn for_group(group_id: i32, policies: Vec<Policy>) -> DconfGroup {
        let mut split_policies: Vec<SplitPolicy> = Vec::new();

        for policy in policies {
            let mut path: Vec<String> = policy.key.split("/").map(|section| {
                String::from(section)
            }).collect();

            let pol: &Value = &policy.value;
            let val: DconfValue = serde_json::from_value(pol.clone()).unwrap();

            split_policies.push(SplitPolicy {
                key: path,
                value: val
            });
        }

        return DconfGroup {
            policies: split_policies
        };
    }

    // Generates a JSON object to send over network for a particular module
    fn generate_asset(&self) -> Value {
        let mut output = serde_json::Map::new();
        // Converts object into a vector containing a vector consisting of the path parts, and the dconf value
        let pols: Vec<(Vec<String>, DconfValue)> = self.into();
        for pol in pols {
            // Extracts the DconfValue
            let dcval = pol.1;

            // Extracts the last part of the key from the vector
            let mut vector = pol.0.clone();
            let key_name = vector.remove(vector.len() - 1);

            // Formats the key URL
            let mut key_url =  String::new();
            while vector.len() > 1 {
                key_url += format!("{}/", vector.remove(0)).as_str();
            }
            key_url += vector.remove(0).as_str();

            match output.remove(&key_url) {
                // Expands upon key pairs already assigned to dconf url
                Some(mut x) => {
                    let mut x = x.as_array().unwrap().clone();
                    x.push(serde_json::json!(
                    DconfPair {
                        key: key_name,
                        value: dcval
                    }));
                    output.insert(key_url, Value::Array(x));
                },
                // Adds a new key pair with dconf url to map
                None => {
                    output.insert(key_url, Value::Array(Vec::from([serde_json::json!(
                    DconfPair {
                        key: key_name,
                        value: dcval
                    })])));
                }
            }
        }
        return Value::from(output);
    }
}