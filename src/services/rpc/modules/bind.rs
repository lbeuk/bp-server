use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use serde_json::Value;
use crate::database::schema::devices::dsl::devices;
use crate::database::models::NewDevice;
use crate::DbPool;
use std::convert::TryFrom;
use diesel::RunQueryDsl;


pub fn bind_meta() -> Value {
    return serde_json::json!({
        "domain": "temp"
    });
}

pub fn bind_client(db_connection: &DbPool, params: &Value) -> Value {

    let permit_binding = true;

    let mut ticket: String = String::from("");
    if permit_binding {
        // Create passcode for client to verify server by
        ticket = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(60)
            .collect();

        let ip_address = i32::try_from(params["from_ip"].as_i64().unwrap()).unwrap();

        // Creates new device entry to inject into database
        let new_device = NewDevice {
            gid: None,
            hn: String::from("Test"),
            sn: None,
            ip: ip_address,
            ticket: ticket.clone()
        };

        diesel::insert_into(devices).values(&new_device).execute(&db_connection.get().unwrap());
    }

    return serde_json::json!({
        "result": permit_binding,
        "ticket": ticket
    });

}

