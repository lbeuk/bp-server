use serde_json::Value;
use bp_conventions::conventions;
use crate::{DbPool, LdapPool};

// Rpc commands per module
use crate::services::rpc::modules::{
    // Bind module - serves to connect new clients to server
    bind::{bind_meta, bind_client},
    // Policy module - serves to manage all policy settings for clients
    policy::{set_policies, get_assets},
    // Auth module - larger module containing authorization and authentication modules
    auth::{
        user::request_user_token
    }
};

// Shorthand type for standardized output format for RPC functions
pub type RpcOut = Result<Value, conventions::rpc::ErrorData>;

// Distributes RPC calls
pub async fn rpc_select(db_state: &DbPool, ldap_state: &LdapPool, method: &str, params: Value) -> RpcOut {
    return match method {

        // Authentication and authorization
        "request_user_token" => request_user_token(&db_state, &ldap_state, &params),
        "request_device_token" => Ok(Value::Null),

        // Bind module
        "bind_meta" => Ok(bind_meta()),
        "bind_client" => Ok(bind_client(&db_state, &params)),

        // Policy module
        "set_policies" => Ok(set_policies(&db_state, &params)),
        "get_assets" => Ok(get_assets(&db_state, &params)),

        // Errors on any non-defined rpc call
        _ => Err(conventions::rpc::ErrorData::std(-32601)),
    }
}

