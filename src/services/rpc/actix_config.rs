use actix_web::{web, HttpResponse, Error, HttpRequest};
use crate::services::rpc::handler;
use serde_json::Value;
use actix_web::web::Bytes;
use bp_conventions::conventions;
use crate::{DbPool, LdapPool};
use std::str::FromStr;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/")
            .route(web::post().to(rpc_handler))
    );
}

async fn rpc_handler(body: Bytes, http_req: HttpRequest, db_state: web::Data<DbPool>, ldap_state: web::Data<LdapPool>) -> Result<HttpResponse, Error> {
    // Returns whether valid request
    let request_json: conventions::rpc::Request = match serde_json::from_slice(body.as_ref()) {
        Ok(ok) => ok,
        Err(_) => {
            let error_body = conventions::rpc::Response {
                jsonrpc: String::from(conventions::rpc::JSONRPC_VERSION),
                result: Value::Null,
                error: Some(conventions::rpc::ErrorData::std(-32700)),
                id: Value::Null,
            };
            // Exits early if the request json could not be parsed
            return Ok(HttpResponse::Ok()
                .content_type("application/json")
                .body(error_body.dump()));
            // Request JSON can be assumed to be parsable from here on
        }
    };

    // Edits paramaters to include necessary request information

    // Processes IP address into i32
    let request_ip_address = String::from(http_req.connection_info().realip_remote_addr().unwrap());
    let port_index = request_ip_address.find(":").unwrap();
    let request_ip_address_cut = &request_ip_address[..port_index];

    let ip_address_octets = std::net::Ipv4Addr::from_str(
        &request_ip_address_cut
    ).unwrap().octets();
    let mut ip_address: i32 = 0;
    let base: i32 = 256;
    let powers = [3,2,1,0];
    for i in 0..4 {
        let oct: i32 = ip_address_octets[i].into();
        ip_address += oct*base.pow(powers[i]);
    }


    // Adds processed information into request
    let mut request = request_json.params.as_object().unwrap().clone();
    request.insert(String::from("from_ip"), Value::from(ip_address));

    // Filters out unauthorized requests


    // Converts request back into JSON value
    let request = Value::from(request);

    // Creates a default response object, fills in with available information
    let mut result = conventions::rpc::Response::default();
    result.id = Value::from(request_json.id.clone());
    match handler::rpc_select(&db_state, &ldap_state, request_json.method.as_str(), request).await {
        Ok(ok) => result.result = ok,
        Err(e) => result.error = Some(e),
    }

    // Returns HTTP response
    return Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(result.dump()));
}