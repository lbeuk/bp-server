//! RPC modules organize different subsets of actions into a hierarchy, based on their function.
//!
//! # Modules
//! * [`policy`]: interacts with the database to manage policy objects to be sent to clients.
//! * [`bind`]: takes incoming bind requests and fulfills operations with requesting client.
//! * [`auth`]: authenticates and authorizes users for requested actions.

pub mod policy;
pub mod bind;
pub mod auth;
